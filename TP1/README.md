# ConvertZone

Suatu ketika saat kamu baru saja terbangun, kamu melihat sekelilingmu menjadi aneh. Anggota keluargamu saling menyapa dengan sapaan lain yang kamu tidak tahu artinya. Masih mencoba memikirkan apa yang terjadi, adikmu menghampiri dan meminta kamu mengerjakan PR nya. “Adik yang aneh”, katamu. Tapi dia tidak mengerti apa yang kamu maksud. Menghindari suasana bingung, kamu cepat mengalihkan perhatian ke PR adikmu. Tapi ternyata soalnya tidak biasa. Lagi-lagi, ada banyak kata yang kamu tidak mengerti. Kamu jadi heran. Kamu berfikir apakah dunia sudah berubah? Ada beberapa hal yang muncul dan hilang. Kamu sebagai pecinta hitung-hitungan dan angka, sangat takut jika angka dan bilangan yang kita kenal sekarang ikut menghilang. Pikiran yang aneh, tapi ada benarnya! Kamu bertindak cepat dan memutuskan untuk belajar bilangan-bilangan lain selain bilangan yang saat ini kamu gunakan, untuk berjaga-jaga. Kamu ingin terus berlatih agar nanti kamu semakin siap jika yang kamu takutkan benar-benar terjadi. Lalu kamu memiliki ide untuk membuat sebuah program untuk membantu kamu belajar. Ya, kamu merasa benar-benar perlu membuatnya. “Aha! ConvertZone”. ConvertZone adalah sebuah game berbasis quiz yang ditujukan untuk melatih kamu melakukan konversi bilangan desimal ke basis lainnya yang akan dijelaskan lebih lanjut pada bagian implementasi.

## Implementasi ConvertZone

ConvertZone memiliki 3 mode dan 1 opsi keluar, yaitu:

1. Decimal ke Quaternary

2. Decimal ke Senary

3. Decimal ke Two’s Complement

4. Quit

Quaternary adalah bilangan berbasis 4, senary adalah bilangan berbasis 6, sedangkan two’s complement adalah representasi lain dari bilangan biner (basis 2).

Semua soal yang akan diberikan dipastikan merupakan bilangan bulat dalam rentang 2 sampai 100 (inklusif). 

```Hint: Modul random digunakan untuk men-generate bilangan bulat secara acak.```

## Aturan main

1. Pada awal permainan, pemain akan mempunyai 3 nyawa.

2. Pemain akan menjawab soal sesuai mode yang dipilih. Jika pemain menjawab dengan benar, maka score akan bertambah 1. Sedangkan jika salah, maka score tetap namun nyawa akan berkurang 1.

3. Pemain akan terus bermain selama masih ada nyawa yang tersisa. Namun pemain juga dapat keluar dari permainan kapanpun dengan memilih opsi keluar (Quit).

4. Jika nyawa habis, maka terdapat beberapa kemungkinan, yaitu:
    
    - Nyawa habis namun score masih tersisa >= 3.

        Pada kasus ini, pemain dapat membeli 1 nyawa dengan menukar 3 poin score. Namun, pemain bisa memilih untuk tidak membeli nyawa dan permainan selesai.

    - Nyawa habis namun score tersisa < 3.

        Pada kasus ini, pemain sudah tidak dapat lagi bermain sehingga permainan juga selesai.

## To Do

- [ ] Implementasikan menu Pilih mode.

- [ ] Implementasikan interaksi permainan sesuai contoh pada Test Case.

- [ ] Terdapat interaksi khusus pada beberapa kasus berikut.

    - [ ] Terdapat perintah untuk memasukkan jawaban dalam 8 bit jika memilih mode 3 (two’s complement)

    - [ ] Program akan mengeluarkan output “Game Over.” jika pemain keluar saat nyawanya telah habis dan mengeluarkan output “Game selesai.” jika keluar dengan memilih opsi Quit.
    
    - [ ] Pada akhir permainan, munculkan total score yang diperoleh pemain.

- [ ] Berikan feedback kepada pemain, baik ketika pemain menjawab benar maupun salah. Dan juga berikan jawaban yang benar jika pemain menjawab salah. 

- [ ] Handle jika input dari user tidak sesuai, yaitu pada kasus berikut

    - [ ] Jika input pada Pilih Mode di luar 1-4 (misal: -1, 0, 5, “ya”, dll.)

    - [ ] Jika jawaban yang diberikan bukan angka (misal: “satu”, “skip”, dll.)

    ```Hint: Fungsi isdigit() digunakan untuk memeriksa apakah suatu string merupakan representasi bilangan atau bukan```

- [ ] Feedback dan proses handling harus sesuai contoh Test Case pada soal.

## Bantuan

Alur jalannya program dapat dilihat [disini](https://drive.google.com/file/d/1tfsGYgwcgUWH1aWar4vVgDxTq8g7ukQk/view?usp=sharing).

## Syarat Pengumpulan

1. Format penamaan file: TP1_[KodeAsdos]\_[NPM]\_[Nama].py

2. File .py dimasukkan ke dalam folder TP1 pada repo GitLab

3. Lakukan prosess add, commit (commit message dibebaskan), dan push ke GitLab seperti pada pengerjaan Lab

4. Pastikan visibility dari repositori-mu adalah private dan telah menambahkan asdos sebagai maintainer
