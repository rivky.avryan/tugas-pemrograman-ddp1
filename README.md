# Git Preparation

DDP1 Genap 2021/2022

### **Khusus TP1:**

1. Fork repository TP DDP1 ini dan pilih opsi private pada visibility repository.

2. Tambahkan akun GitLab milik asdos kamu dan pilih role maintainer.

3. Clone repository dengan URL HTTPS pada direktori lokal komputermu yang kamu inginkan.

4. Kerjakan Tugas Pemrograman pada folder yang sesuai. Misal Tugas Pemrograman 1 pada folder TP1. Ikuti syarat pengumpulan yang ada pada soal TP terkait.

5. Tambahkan remote baru bernama upstream dengan command berikut

    ```git remote add upstream https://gitlab.com/ddp1-genap-2021-2022/tugas-pemrograman-ddp1.git```

6. Lakukan proses add, commit, dan push.

### **Pengerjaan TP2 dan seterusnya:**

1. Jalankan `git pull upstream master`.

2. Kerjakan Tugas Pemrograman sesuai ketentuan pada soal.

3. Lakukan proses add, commit, dan push.

